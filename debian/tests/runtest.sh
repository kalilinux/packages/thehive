#!/bin/sh

set -e

thehive-start

# test if the service started
if ! systemctl is-active -q thehive; then
    echo "FAILURE: the service thehive is not active"
    systemctl status thehive
    exit 1
fi

# thehive is very long to start the first time
sleep 200s

# test Web UI response
if ! curl -s http://127.0.0.1:9000/index.html | grep "TheHive"; then
    echo "FAILURE: Web UI test fails"
    exit 1
fi

# stop the service
thehive-stop

if systemctl is-active -q thehive; then
    echo "FAILURE: the service thehive is still active"
    exit 1
fi

thehive-stop
